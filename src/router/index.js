import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/admin-panel",
    name: "admin-panel",
    component: () => import("@/views/admin-panel"),
    meta: { title: "Главная" },
  },
  {
    path: "/",
    name: "Auth",
    component: () => import("@/views/auth"),
    meta: { title: "Авторизация", layout: "auth-layout" },
  },
  {
    path: "/statistics",
    name: "statistics",
    component: () => import("@/views/routes/statistics"),
    meta: { title: "Статистика" },
  },
  {
    path: "/informational-resources",
    name: "informational-resources",
    component: () => import("@/views/routes/informational-resources"),
    meta: { title: "Информационные ресурсы" },
  },
  {
    path: "/banners",
    name: "banners",
    component: () => import("@/views/routes/banners"),
    meta: { title: "Баннеры" },
  },
  {
    path: "/sliders",
    name: "sliders",
    component: () => import("@/views/routes/sliders"),
    meta: { title: "Результаты" },
  },
  {
    path: "/menus",
    name: "menus",
    component: () => import("@/views/routes/menus"),
    meta: { title: "Меню" },
  },
  {
    path: "/actual-themes",
    name: "actual-themes",
    component: () => import("@/views/routes/actual-themes"),
    meta: { title: "Актуальные темы " },
  },
  {
    path: "/fragments",
    name: "fragments",
    component: () => import("@/views/routes/fragments"),
    meta: { title: "Фрагменты " },
  },
  {
    path: "/vacancies",
    name: "vacancies",
    component: () => import("@/views/routes/vacancies"),
    meta: { title: "Вакансии " },
  },
  {
    path: "/hot-companies",
    name: "companies",
    component: () => import("@/views/routes/actual-companies"),
    meta: { title: "Компании " },
  },
  {
    path: "/hot-docs",
    name: "docs",
    component: () => import("@/views/routes/actual-docs"),
    meta: { title: "Документы " },
  },

  // router items

  {
    path: "/menus/:id",
    name: "menus-item",
    component: () => import("@/views/routes-items/menus-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/statistics/:id",
    name: "statistics-item",
    component: () => import("@/views/routes-items/statistics-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/themes/:id",
    name: "themes-item",
    component: () => import("@/views/routes-items/themes-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/companies/:id",
    name: "companies-item",
    component: () => import("@/views/routes-items/companies-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/resources/:id",
    name: "resources-item",
    component: () => import("@/views/routes-items/resources-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/banners/:id",
    name: "banners-item",
    component: () => import("@/views/routes-items/banners-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/results/:id",
    name: "results-item",
    component: () => import("@/views/routes-items/results-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/fragments/:id",
    name: "fragments-item",
    component: () => import("@/views/routes-items/fragments-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/vacancies/:id",
    name: "vacancies-item",
    component: () => import("@/views/routes-items/vacancies-item"),
    meta: { title: "Редактировать" },
  },
  {
    path: "/docs/:id",
    name: "docs-item",
    component: () => import("@/views/routes-items/docs-item"),
    meta: { title: "Редактировать" },
  },

  // create items
  {
    path: "/create-menus-items",
    name: "create-menus-items",
    component: () => import("@/views/create-items-pages/create-menus-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-statistics-items",
    name: "create-statistics-items",
    component: () =>
      import("@/views/create-items-pages/create-statistics-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-themes-items",
    name: "create-themes-items",
    component: () => import("@/views/create-items-pages/create-themes-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-companies-items",
    name: "create-companies-items",
    component: () =>
      import("@/views/create-items-pages/create-companies-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-resources-items",
    name: "create-resources-items",
    component: () =>
      import("@/views/create-items-pages/create-resources-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-banners-items",
    name: "create-banners-items",
    component: () => import("@/views/create-items-pages/create-banners-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-results-items",
    name: "create-results-items",
    component: () => import("@/views/create-items-pages/create-results-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-fragments-items",
    name: "create-fragments-items",
    component: () =>
      import("@/views/create-items-pages/create-fragments-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-vacancies-items",
    name: "create-vacancies-items",
    component: () =>
      import("@/views/create-items-pages/create-vacancies-items"),
    meta: { title: "Создать" },
  },
  {
    path: "/create-docs-items",
    name: "create-docs-items",
    component: () => import("@/views/create-items-pages/create-docs-items"),
    meta: { title: "Создать" },
  },

  // 404

  {
    path: "*",
    name: "404",
    component: () => import("@/views/404"),
    meta: { title: "404 Not Found" },
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
