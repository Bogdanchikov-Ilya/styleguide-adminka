import MainServices from "@/services/MainServices";
export default {
  data() {
    return {
      MainServices: new MainServices(),
    };
  },
  methods: {
    getItemsNumber() {
      return Math.round(window.innerHeight / 51) - 2;
    },
  },
};
