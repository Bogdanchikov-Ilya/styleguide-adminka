import { api } from "../api/api";
import store from "../store";
import router from "../router";

class AuthServices {
  async sendAuth(item) {
    try {
      store.commit("setTrue");
      const result = await api.post("/api/login", item);
      if (result.status === 200) {
        // записываю токен и перекидываю в админку
        localStorage.setItem(
          "94a08da1fecbb6e8b46990538c7b50b2",
          result.data.access_token
        );
        localStorage.setItem("username", result.data.user.name);
        window.axios.defaults.headers.common["Authorization"] =
          "Bearer " + result.data.access_token;

        store.commit("setName", result.data.user.name);
        store.commit("setError", false);
        router.push("/admin-panel");
      }
      return result.data;
    } catch (e) {
      e ? store.commit("setError", true) : store.commit("setError", false);
      throw e;
    } finally {
      store.commit("setFalse");
    }
  }
  async logout() {
    try {
      store.commit("setTrue");
      api.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("94a08da1fecbb6e8b46990538c7b50b2");
      const result = await api.post("/api/logout");
      if (result) {
        localStorage.clear();
      }
    } catch (e) {
      throw e;
    } finally {
      store.commit("setFalse");
      localStorage.clear();
      router.push("/");
    }
  }
  async refresh() {
    api.defaults.headers.common["Authorization"] =
      "Bearer " + localStorage.getItem("94a08da1fecbb6e8b46990538c7b50b2");
    const result = await api.post("/api/refresh");
    if (result.data.access_token) {
      localStorage.setItem(
        "94a08da1fecbb6e8b46990538c7b50b2",
        result.data.access_token
      );
      localStorage.setItem("username", result.data.user.name);
    } else {
      router.push("/");
    }
  }
  getObj() {
    if (store.getters.getObj !== null) {
      return store.getters.getObj;
    }
  }
}

// функция чтобы распарсить токен и взять из него время жизни
function parseJwt(token) {
  let base64Url = token.split(".")[1];
  let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  let jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
}

// проверка токена при каждом перехоже на роут в котором нужна аворизация
router.beforeEach((to, from, next) => {
  if (to.name !== "Auth") {
    const token = localStorage.getItem("94a08da1fecbb6e8b46990538c7b50b2");
    if (token) {
      const decoded = parseJwt(token);
      let shelfLife = decoded.exp * 1000;
      let curretDate = new Date();
      if (shelfLife < curretDate.getTime()) {
        // если протух
        this.refresh();
      } else {
        // если токен НЕ протух
        next();
      }
    } else {
      router.push("/");
    }
  } else next();
});

export default AuthServices;
