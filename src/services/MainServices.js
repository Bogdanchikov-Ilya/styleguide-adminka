import { api } from "../api/api";
import store from "../store";

const token = localStorage.getItem("94a08da1fecbb6e8b46990538c7b50b2");
api.defaults.headers.common["Authorization"] = "Bearer " + token;

class MainServices {
  storeItems(url, items, lazyLoad) {
    switch (url) {
      case "api/menus":
        lazyLoad
          ? store.commit("setLazyLoadMenus", items)
          : store.commit("setMenus", items);
        break;
      case "api/hot_topics":
        lazyLoad
          ? store.commit("setLazyLoadThemes", items)
          : store.commit("setThemes", items);
        break;
      case "api/results":
        lazyLoad
          ? store.commit("setLazyLoadResults", items)
          : store.commit("setResults", items);
        break;
      case "api/statistics":
        lazyLoad
          ? store.commit("setLazyLoadStatistics", items)
          : store.commit("setStatistics", items);
        break;
      case "api/fragments":
        lazyLoad
          ? store.commit("setLazyLoadFragments", items)
          : store.commit("setFragments", items);
        break;
      case "api/resources":
        lazyLoad
          ? store.commit("setLazyLoadResources", items)
          : store.commit("setResources", items);
        break;
      case "api/banners":
        lazyLoad
          ? store.commit("setLazyLoadBanners", items)
          : store.commit("setBanners", items);
        break;
      case "api/vacancies":
        lazyLoad
          ? store.commit("setLazyLoadVacancies", items)
          : store.commit("setVacancies", items);
        break;
      case "api/hot_companies":
        lazyLoad
          ? store.commit("setLazyLoadCompanies", items)
          : store.commit("setCompanies", items);
        break;
      case "api/hot_docs":
        lazyLoad
          ? store.commit("setLazyLoadDocs", items)
          : store.commit("setDocs", items);
        break;
    }
  }

  async loadItems(url, pagination = "") {
    store.commit("setTrue");
    try {
      const result = await api.get(url + pagination);
      if (result.status !== 200) return false;
      return result.data;
    } catch (e) {
      store.commit("setNotification", {
        status: false,
        text: "Ошибка при загрузке данных",
      });
      throw e;
    } finally {
      store.commit("setFalse");
    }
  }

  async lazyLoadItems(url, pagination = "") {
    let test;
    try {
      const result = await api.get(url + pagination);
      test = result.data;
      return result.data;
    } catch (e) {
      throw e;
    } finally {
      if (test.length) this.storeItems(url, test, true);
    }
  }

  async loadById(url, id) {
    store.commit("setTrue");
    try {
      const result = await api.get(url + "/" + id);
      return result.data;
    } catch (e) {
      throw e;
    } finally {
      store.commit("setFalse");
    }
  }

  async create(item, url) {
    store.commit("setTrue");
    try {
      const result = await api.post(url, item);
      if (result.status === 200 || result.status === 201)
        store.commit("setNotification", {
          status: true,
          text: "Изменения сохранены",
        });
      return result.data;
    } catch (e) {
      store.commit("setNotification", {
        status: false,
        text: "Ошибка при создании",
      });
      throw e;
    } finally {
      this.storeItems(url, [], false);
    }
  }

  async update(item, id, url, withFile) {
    store.commit("setTrue");
    try {
      let result;
      withFile
        ? (result = await api.post(url + "/" + id, item))
        : (result = await api.put(url + "/" + id, item));
      if (result.status === 200 || result.status === 201)
        this.loadItems(url).then((items) => this.storeItems(url, items));
      store.commit("setNotification", {
        status: true,
        text: "Изменения сохранены",
      });
      return result.data;
    } catch (e) {
      store.commit("setNotification", {
        status: false,
        text: "Изменения не сохранены",
      });
      throw e;
    } finally {
      this.storeItems(url, [], false);
    }
  }

  async delete(id, url) {
    store.commit("setTrue");
    try {
      const result = await api.delete(url + "/" + id);
      if (
        result.status === 200 ||
        result.status === 201 ||
        result.status === 204
      )
        store.commit("setNotification", {
          status: true,
          text: "Успешно удалено",
        });
      return result.data;
    } catch (e) {
      store.commit("setNotification", {
        status: false,
        text: "Ошибка при удалении",
      });
      throw e;
    } finally {
      this.loadItems(url).then((items) => this.storeItems(url, items));
      store.commit("setItem", null);
    }
  }

  getItems(url, pagination) {
    switch (url) {
      case "api/menus":
        if (!store.getters.getMenus)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getMenus;
      case "api/hot_topics":
        if (!store.getters.getThemes)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getThemes;
      case "api/results":
        if (!store.getters.getResults)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getResults;
      case "api/statistics":
        if (!store.getters.getStatistics)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getStatistics;
      case "api/fragments":
        if (!store.getters.getFragments)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getFragments;
      case "api/resources":
        if (!store.getters.getResources)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getResources;
      case "api/banners":
        if (!store.getters.getBanners)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getBanners;
      case "api/vacancies":
        if (!store.getters.getVacancies)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getVacancies;
      case "api/hot_companies":
        if (!store.getters.getCompanies)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getCompanies;
      case "api/hot_docs":
        if (!store.getters.getDocs && pagination)
          this.loadItems(url, pagination).then((items) =>
            this.storeItems(url, items)
          );
        return store.getters.getDocs;
    }
  }

  getItem(url, id) {
    if (!store.getters.getItem)
      this.loadById(url, id).then((item) => store.commit("setItem", item));
    return store.getters.getItem;
  }
}

export default MainServices;
