export default {
  state: {
    companies: [],
    companiesPageCounter: 1,
  },
  mutations: {
    setCompanies(state, data) {
      state.companies = data;
    },
    addCompaniesPageCounter(state, data) {
      state.companiesPageCounter = data;
    },
    setLazyLoadCompanies(state, data) {
      state.companies = Array.from(new Set(state.companies.concat(data))); // удлаяю дубликаты
    },
  },
  getters: {
    getCompanies(state) {
      if (state.companies.length) {
        return state.companies; // удлаяю дубликаты
      }
    },
    getCompaniesPageCounter(state) {
      return state.companiesPageCounter;
    },
  },
};
