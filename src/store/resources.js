export default {
  state: {
    resources: [],
  },
  mutations: {
    setResources(state, data) {
      state.resources = data;
    },
    setLazyLoadResources(state, data) {
      let arr = state.resources.concat(data);
      state.resources = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getResources(state) {
      if (state.resources.length) {
        return state.resources;
      }
    },
  },
};
