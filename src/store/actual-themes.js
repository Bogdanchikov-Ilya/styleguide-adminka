export default {
  state: {
    themes: [],
  },
  mutations: {
    setThemes(state, data) {
      state.themes = data;
    },
    setLazyLoadThemes(state, data) {
      let arr = state.themes.concat(data);
      state.themes = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getThemes(state) {
      if (state.themes.length) {
        return state.themes;
      }
    },
  },
};
