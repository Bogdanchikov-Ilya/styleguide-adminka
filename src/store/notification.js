export default {
  state: {
    showNotification: false,
    isError: null,
    notificationText: "",
  },
  mutations: {
    setNotification(state, payload) {
      state.showNotification = true;
      state.isError = payload.status;
      state.notificationText = payload.text;
      setTimeout(() => {
        state.showNotification = false;
      }, 3000);
    },
  },
  getters: {
    getNotification(state) {
      return {
        show: state.showNotification,
        status: state.isError,
        text: state.notificationText,
      };
    },
  },
};
