export default {
  state: {
    docs: [],
    docsCounter: 1,
  },
  mutations: {
    setDocs(state, data) {
      state.docs = data;
    },
    addDocsCounter(state, data) {
      state.docsCounter = data;
    },
    setLazyLoadDocs(state, data) {
      state.docs = Array.from(new Set(state.docs.concat(data)));
    },
  },
  getters: {
    getDocs(state) {
      if (state.docs.length) {
        return state.docs;
      }
    },
    getDocsCounter(state) {
      return state.docsCounter;
    },
  },
};
