export default {
  state: {
    name: null,
    error: null,
  },
  mutations: {
    setName(state, data) {
      state.name = data;
    },
    setError(state, data) {
      state.error = data;
    },
  },
  getters: {
    getName(state) {
      return state.name;
    },
    getError(state) {
      return state.error;
    },
  },
};
