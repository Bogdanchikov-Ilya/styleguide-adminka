export default {
  state: {
    fragments: [],
  },
  mutations: {
    setFragments(state, data) {
      state.fragments = data;
    },
    setLazyLoadFragments(state, data) {
      let arr = state.fragments.concat(data);
      state.fragments = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getFragments(state) {
      if (state.fragments.length) {
        return state.fragments;
      }
    },
  },
};
