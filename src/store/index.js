import Vue from "vue";
import Vuex from "vuex";

import statistics from "./statistics";
import resources from "./resources";
import banners from "./banners";
import results from "./results";
import menus from "./menus";
import actualThemes from "./actual-themes";
import fragments from "./fragments";
import vacancies from "./vacancies";
import companies from "./companies";
import docs from "./docs";

import notification from "./notification";
import auth from "./auth";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    url: process.env.VUE_APP_DOMEN,
    // sidebar showStatus
    showSidebar: true,
    // detailItem
    item: null,
    // preloader status
    loader: false,
  },
  mutations: {
    //set sidebar showStatus
    setSidebar(state) {
      state.showSidebar = !state.showSidebar;
    },
    // set detail item
    setItem(state, data) {
      state.item = data;
    },
    // set preloader status
    setFalse(state) {
      state.loader = false;
    },
    setTrue(state) {
      state.loader = true;
    },
  },
  getters: {
    getApiUrl(state) {
      return state.url;
    },
    //sidebar showStatus
    getSidebar(state) {
      return state.showSidebar;
    },
    //get detail item
    getItem(state) {
      return state.item;
    },
    //  get Preloader status
    getPreloaderStatus(state) {
      return state.loader;
    },
  },
  modules: {
    statistics,
    resources,
    banners,
    results,
    menus,
    actualThemes,
    fragments,
    vacancies,
    companies,
    docs,
    notification,
    auth,
  },
});
