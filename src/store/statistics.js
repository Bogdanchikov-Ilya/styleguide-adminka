export default {
  state: {
    statistics: [],
  },
  mutations: {
    setStatistics(state, data) {
      state.statistics = data;
    },
    setLazyLoadStatistics(state, data) {
      let arr = state.statistics.concat(data);
      state.statistics = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getStatistics(state) {
      if (state.statistics.length) {
        return state.statistics;
      }
    },
  },
};
