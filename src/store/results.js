export default {
  state: {
    results: [],
  },
  mutations: {
    setResults(state, data) {
      state.results = data;
    },
    setLazyLoadResults(state, data) {
      let arr = state.results.concat(data);
      state.results = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getResults(state) {
      if (state.results.length) {
        return state.results;
      }
    },
  },
};
