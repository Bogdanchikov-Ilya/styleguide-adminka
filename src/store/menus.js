export default {
  state: {
    menus: [],
  },
  mutations: {
    setMenus(state, data) {
      state.menus = data;
    },
    setLazyLoadMenus(state, data) {
      let arr = state.menus.concat(data);
      state.menus = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getMenus(state) {
      if (state.menus.length) {
        return state.menus;
      }
    },
  },
};
