export default {
  state: {
    vacancies: [],
  },
  mutations: {
    setVacancies(state, data) {
      state.vacancies = data;
    },
    setLazyLoadVacancies(state, data) {
      let arr = state.vacancies.concat(data);
      state.vacancies = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getVacancies(state) {
      if (state.vacancies.length) {
        return state.vacancies;
      }
    },
  },
};
