export default {
  state: {
    banners: [],
  },
  mutations: {
    setBanners(state, data) {
      state.banners = data;
    },
    setLazyLoadBanners(state, data) {
      let arr = state.banners.concat(data);
      state.banners = arr.filter((e, i, a) => a.indexOf(e) === i);
    },
  },
  getters: {
    getBanners(state) {
      if (state.banners.length) {
        return state.banners;
      }
    },
  },
};
