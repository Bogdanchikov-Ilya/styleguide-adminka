import axios from "axios";

export var api = axios.create({
  headers: {
    "Content-type": "application/json",
  },
  baseURL: process.env.VUE_APP_DOMEN,
  responseType: "json",
});
