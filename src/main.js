import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./index.css";
import Paginate from "vuejs-paginate";
import CKEditor from "@ckeditor/ckeditor5-vue2";

import axios from "axios";
window.axios = require("axios");

// layouts
import sidebarLayout from "./layouts/sidebar-layout";
import authLayout from "./layouts/auth-layout";

Vue.component("sidebar-layout", sidebarLayout);
Vue.component("auth-layout", authLayout);

Vue.config.productionTip = false;
Vue.use(CKEditor);
Vue.component("Paginate", Paginate);

new Vue({
  router,
  store,
  axios,
  render: (h) => h(App),
}).$mount("#app");
