module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      blue: "#5988FF", // акцентный
      lightBlue: "#7f9df6",
      white: "#fff",
      gray: "#4F6576", // хз
      lightGray: "#808080", // для не важного текста
      black: "#1F1F1F", // акцентный для текста
      red: "#ef2525",
      lightRed: "#e05454",
      preWhite: "#e8e8e8",
    },
    fontSize: {
      xs: ".75rem",
      sm: ".875rem",
      tiny: ".875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
      "7xl": "5rem",
      "20px": "1.042vw",
      "30px": "1.563vw",
    },
    extend: {
      width: {
        "1/7": "14.2857143%",
        "2/7": "28.5714286%",
        "3/7": "42.8571429%",
        "4/7": "57.1428571%",
        "5/7": "71.4285714%",
        "6/7": "85.7142857%",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
